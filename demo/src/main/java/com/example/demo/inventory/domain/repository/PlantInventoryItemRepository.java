package com.example.demo.inventory.domain.repository;

import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {
    PlantInventoryItem findOneByPlantInfo(PlantInventoryEntry entry);


	String IS_AVAILABLE = "" +
			"SELECT EXISTS(" +
			"SELECT p FROM plant_inventory_item p " +
			"AND p.equipment_condition = 'SERVICEABLE' " +
			"AND p.id NOT IN (SELECT po.plant_id " +
			"FROM purchase_order po " +
			"WHERE (:START_DATE < po.end_date AND :END_DATE > po.start_date) " +
			"po.id = :ID " +
			"AND po.status IN ('ACCEPTED', 'DISPATCHED') " +
			"))";

	@Query(nativeQuery = true, value = IS_AVAILABLE)
	boolean isAvailable(@Param("ID") Long itemId, @Param("START_DATE") Date startDate, @Param("END_DATE") Date endDate);

}
