package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

	@Autowired
	PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

	@Autowired
	InventoryService inventoryService;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

	PlantInventoryEntry plantInventoryEntry;

    public PurchaseOrderDTO findPO(Long id) {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                         poDTO.getRentalPeriod().getStartDate(),
                         poDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid PO Period");

        if(poDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        poRepository.save(po);

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getId(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if(availableItems.size() == 0) {
            po.setStatus(POStatus.REJECTED);
            throw new Exception("No available items");
        }

        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(po.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));

        plantReservationRepository.save(reservation);
        po.getReservations().add(reservation);

        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);

    }

    public PurchaseOrderDTO acceptPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        po.setStatus(POStatus.OPEN);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPO(Long id) throws Exception {
        PurchaseOrder po = getPO(id);
        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.CLOSED);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    private PurchaseOrder getPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        return po;
    }

    public ResponseEntity<PurchaseOrderDTO> modifyPO(Long id, PurchaseOrderDTO dto) throws Exception {

    	PurchaseOrder po = getPO(id);

    	if(po == null){
		    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }

    	POStatus current = po.getStatus();
    	BusinessPeriod old = po.getRentalPeriod();

	    if (current == POStatus.ACCEPTED &&
			    (old.getStartDate().isAfter(LocalDate.now()) &&
					    dto.getRentalPeriod().getStartDate().isAfter(LocalDate.now()))
	    ) {
		    BusinessPeriod period = BusinessPeriod.of(old.getStartDate(), dto.getRentalPeriod().getEndDate());
		    if (!isItemStillAvailableWithNewPeriod(po, period)) {
			    List<PlantInventoryEntryDTO> availablePlants = inventoryService.findAvailablePlants("", period.getStartDate(), period.getEndDate());

			    if (availablePlants.size() < 1)
				    return new ResponseEntity<>(HttpStatus.CONFLICT);
			    else {
					PlantInventoryEntryDTO pie = availablePlants.get(0);
				    po.setPlant(PlantInventoryEntry.of(pie.get_id(), pie.getName(), pie.getDescription(), pie.getPrice()));
			    }
		    }
		    po.setStatus(POStatus.EXTENSION_PENDING);
		    po.setRentalPeriod(period);
		    poRepository.save(po);

	    }

	    return new ResponseEntity<>(purchaseOrderAssembler.toResource(po), HttpStatus.OK);

    }

	private boolean isItemStillAvailableWithNewPeriod(PurchaseOrder po, BusinessPeriod period) {
		POStatus current = po.getStatus();
		BusinessPeriod old = po.getRentalPeriod();

		po.setRentalPeriod(period);
		po.setStatus(POStatus.CANCELLED);
		poRepository.save(po);

		boolean available = isItemStillAvailable(po);

		po.setRentalPeriod(old);
		po.setStatus(current);
		poRepository.save(po);

		return available;
	}

	private boolean isItemStillAvailable(PurchaseOrder po) {
		return plantInventoryItemRepository.isAvailable(
				po.getId(),
				Date.valueOf(po.getRentalPeriod().getStartDate()),
				Date.valueOf(po.getRentalPeriod().getEndDate())
		);
	}


}
