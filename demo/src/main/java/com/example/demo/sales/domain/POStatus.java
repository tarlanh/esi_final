package com.example.demo.sales.domain;

public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED, ACCEPTED, CANCELLED, EXTENSION_PENDING;
}
